import modal from './modules/modal.js';
import webSocket from './modules/webSocketConnection.js';

/* Variables and Const declaration */
const parentContainer = document.querySelector(".documents__container");
let view = 'list'
let documents = "";



webSocket.notificationsWebSocket();



/* Modal operations*/
console.log(document.body)
//Model form to add new Documents
const submitFormEventListener = function() {
    document.querySelector("#form-new-document").addEventListener("submit", (event)=> {
    event.preventDefault();
    const contributors = window.document.querySelector('#contributors').value.split(' ');
    const attachments = Object.values(event.target['attachments'].files)

    const document = {
        Title: window.document.querySelector('#name').value,
        Version: window.document.querySelector('#version').value,
        Contributors: contributors.map(contributor => {
            return {Name: contributor}
        }),
        Attachments: attachments.map(value => [value.name])
    }
    modal.addDocument(document, view);

})
};
submitFormEventListener();




/*I nteractions to change de layout */

const toggleView = (viewType) => {

    view = viewType;
    const addButtonList = document.querySelector("#newDocument");
    if(viewType === "grid") {

        /* Hide the header section of documents data */
        const headerTitles = document.querySelector("#documentsHeader");
        headerTitles.style.display = "none"

        /* Change de color of the button to show which is checked*/
        document.querySelector('.view-grid-icon').style.color = 'black';
        document.querySelector('.view-list-icon').style.color = 'grey';


        /* Change classes to the opposite layout*/
        const documentContainer = document.querySelector(".documents__container__list");
        const documents = document.querySelectorAll(".layout__list");

        documentContainer.classList.remove("documents__container__list");
        documentContainer.classList.add("documents__container__grid");

        documents.forEach(element => {
            element.classList.remove("layout__list");
            element.classList.add("layout__grid")
        });

        
        addButtonList.remove();

        addButtonList.classList.remove("document__new");
        addButtonList.classList.add("document__new__grid");

        documentContainer.insertAdjacentElement("afterend", addButtonList)


    } else if (viewType === "list") {

        const headerTitles = document.querySelector("#documentsHeader");
        headerTitles.removeAttribute("style");

        /* Change de color of the button to show which is checked*/
        document.querySelector('.view-grid-icon').style.color = 'grey';
        document.querySelector('.view-list-icon').style.color = 'black';
        
        /* Change classes to the opposite layout*/
        const documentContainer = document.querySelector(".documents__container__grid");
        const documents = document.querySelectorAll(".layout__grid");

        documentContainer.classList.remove("documents__container__grid");
        documentContainer.classList.add("documents__container__list");

        documents.forEach(element => {
            element.classList.remove("layout__grid");
            element.classList.add("layout__list")
        });

        addButtonList.remove();

        addButtonList.classList.remove("document__new__grid");
        addButtonList.classList.add("document__new");

        documentContainer.appendChild(addButtonList);

        
    }

}

document.querySelector('.view-list-icon').addEventListener('click', () => {
    toggleView('list')
});

document.querySelector('.view-grid-icon').addEventListener('click', () => {
    toggleView('grid')
});



/* Initial data load */

const downloadInitialData = () => {
    fetch("http://localhost:8080/documents")
    .then(res => res.json())
    .then((dataRes) => {

        const data = dataRes.sort((d1, d2) => new Date(d2.CreatedAt) - new Date(d1.CreatedAt));
         data.forEach((element, index) => {
             
            documents += `
            <div class="layout__list" id='document${index}'>

                <div class="document__content__name"> ${element.Title}</div>
                <div class="document__content__contributors">
                    ${element.Contributors.map(contributor => {
                        return `<span>${contributor.Name}</span>`
                    }).join('')}
                </div>
                <div class="document__content__attachments">
                ${element.Attachments.map(attachment => {
                    return `<span>
                        ${attachment}
                    </span>
                    `
                }).join('')}
                </div>
            </div>`
        })

        

        console.log(data);
    }
    )
    .finally(() => {
        parentContainer.innerHTML += `
            
            <div>
            <div class="documents__container__list">
            ${documents}
            <div class="document__new" id="newDocument">
                    <span id="addDocument">+ Add document</span>
                </div>
            </div>
            </div>
            
        `;
        const addNewDocument = document.querySelector("#newDocument");
        addNewDocument.addEventListener("click", () => modal.openForm());
    })
}

downloadInitialData();

export {view}






