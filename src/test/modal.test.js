
import modal from '../modules/modal.js'

let newDocument; 


document.body.innerHTML = `

    <header>
        <div id="notifications" class="notifications">
            <span class="notification__count">0</span>
            <i class="fas fa-bell"></i>
            <span>
                
                New document:           
                <span id="notification-new">

                </span>
                added
            
            </span>
        </div>
        <nav class="documents__menu">
            <h1 class="documents__title">Documents</h1>
            <div class="documents__filters">
                <span>Filtered by: name</span>
                <div class="documents__views">
                    <div class="view-list">
                        <span class="view-list-icon view-icon fas fa-list"></span>
                    </div>
                    <div class="view-grid">
                        <span class="view-grid-icon view-icon fas fa-th-large"></span>
                    </div>

                </div>
            </div>
        </nav>
    </header>

    <div id="modal" class="modalContainer">
        <div class="modal-content">
            <span class="close">×</span>
            <h2>New Document Form</h2>
            <form id="form-new-document" class="form-new-document">
                <label for="name">
                    <p><b>Name</b></p>
                    <input type="text" id="name"/>
                </label>
                <label for="version">
                    <p><b>Version</b></p>
                    <input type="text" id="version"/>
                </label>
                <label for="contributors">
                    <p><b>Contributor</b></p>
                    <input type="text" id="contributors"/>
                </label>
                <label for="attachments">
                    <p><b>Attachments</b></p>
                    <input type="file" id="attachments" name='attachments' multiple="multiple"/>
                </label>
                <button type="submit" class="submit">
                    <i class="fas fa-plus"></i> Add
                </button>
            </form>
        </div>
    </div>
    


    <div class="documents__container">
        <div class="documents__header" id=documentsHeader>
            <div class="documents__header__name">
                <span>Name</span>
            </div>
            <div class="documents__header__name">Contributors</div>
            <div class="documents__header__name">Attachments</div>
        </div>

   `

beforeEach(() => {
    newDocument = {
        Title : 'titulo',
    Contributors: [{Name :'uno dos'}],
    Attachments: ['attach1']
    }
}) 


it('openForm', () => {
    
    modal.openForm();
    const modalForm = document.querySelector('#modal');
    expect(modalForm).not.toBeNull();

});

it('addDocument test', () => {
    document.body.innerHTML += `<div>
    <div class="documents__container__list">
        <div class="layout__list" id="document">

            <div class="document__content__name"> Ruination IPA</div>
            <div class="document__content__contributors">
                <p> Lue Considine</p>
                <p>Vinie welch</p>
                <p>Assa Breke</p>
            </div>
            <div class="document__content__attachments"> Ligh buzz</div>

        </div>
        <div class="layout__list" id="document">

            <div class="document__content__name"> Ruination IPA</div>
            <div class="document__content__contributors">
                <p> Lue Considine</p>
                <p>Vinie welch</p>
                <p>Assa Breke</p>
            </div>
            <div class="document__content__attachments"> Ligh buzz</div>

        </div>
        <div class="document__new" id="newDocument">
            <span id="addDocument">+ Add document</span>
        </div>
    </div>
    
</div>`
    modal.addDocument(newDocument, 'list')
    const expectation = document.querySelectorAll(".document__content__contributors").length
    expect(expectation).toBe(3);
})
