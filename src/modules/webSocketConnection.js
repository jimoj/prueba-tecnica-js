const webSocket = {

    notificationsWebSocket : () => {
    
        const notificationsCount = document.querySelector('.notification__count');
        const newNotificationName = document.querySelector('#notification-new');
        const webSocket= new WebSocket('ws://localhost:8080/notifications');
        let count = 0;
    
        webSocket.onmessage = (notification) => {
            notification.preventDefault();
            count = count + 1;
            const notificationData = JSON.parse(notification.data);
            newNotificationName.innerHTML = `"${notificationData.DocumentTitle}"`;
            notificationsCount.innerHTML = `${count}`
        }
    
    
    }
}


export default webSocket;