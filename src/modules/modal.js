

let modal = {


    addDocument : function(doc, view) {

        const documents = document.querySelectorAll(`.layout__${view}`);
        const container = document.querySelector(`.documents__container__${view}`);
    
        /* Creation of the container to be attached to the layout*/
        const documentToAdd = `<div class="layout__${view}" id='document${documents.length}'>
    
        <div class="document__content__name"> 
            <span> ${doc.Title}</span>
            <span class='subtext'> ${doc.Version}</span>
        </div>
        <div class="document__content__contributors">
            ${doc.Contributors.map(contributor => {
                return `<span>${contributor.Name}</span>`
            }).join('')}
        </div>
        <div class="document__content__attachments">
        ${doc.Attachments.map(attachment => {
            return `<span>
                ${attachment}
            </span>
            `
        }).join('')}
        </div>
        </div>`
    
        
        container.insertAdjacentHTML('afterbegin', documentToAdd);
        this.closeForm();
        this.scrollToTop();
        
    },




    openForm : function (){
    const modal = document.querySelector('#modal');
    const close = document.querySelector('.close');
    modal.style.display = 'block';

    close.onclick = () => {
        this.closeForm();
    }
},

closeForm :() => {
    const modal = document.querySelector('#modal');
    modal.style.display = 'none';

    document.querySelector('#name').value = '';
    document.querySelector('#version').value = '';
    document.querySelector('#contributors').value = '';
    document.querySelector('#attachments').value = '';
},

scrollToTop : () => {
    window.scrollTo({top: 0, behavior: 'smooth'});
  }

}

export default modal;

    
