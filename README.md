# Prueba Tecnica JS

## Authors 
Jaime Jimenez Moreno




## Introduction
This is a web app build with Javascript. It displays a list of documents in two different types of layout. These documents are ordered by date from most recent to the oldest.
We can also create new documents by clcking in the link "Add new document";


It also notificates the user when a new document has been created from outside by displaying and advice at the top section of the page with also, a counter of the documents created.

The data is exposed by an external backend app which give us the content via http and websocket. 

API: [HOLDED] (https://github.com/holdedlab/frontend-challenge)  

  


## Getting started

To help us with the coding of this app we use webpack, to bundle the code and in development mode to have the funcionality of live reloading. This means see the changes in the browser instantly after we save the changes in the code.

To run webpack we have to first have installed npm and node. Then execute the following commands on the temrinal in the root of the project:
```
npm i // To install dependencies
npm run start // To execute and run up the application

```

The server starts at http://localhost:5500/

Also Jest is set up, to test the modules that the package has.
To run Jest:

```
npm run test:coverage 

```
This command above execute jest, and generate the coverage report. This report is created automatically inside the named "coverage" folder in the root of the project. If you want to see the report only have to open the index.html file inside.





## Posible improvements

If we would want to continue adding new features to this application, is recommendable to use some kind of framework which helps with the scalability. Also improve the modularity of the files in it, maybe building components to reuse them in other parts of the application. 
